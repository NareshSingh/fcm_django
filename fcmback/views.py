from django.shortcuts import render
from fcm_django.models import FCMDevice
from django.shortcuts import get_object_or_404
from .forms import *

def home(request):
    devices = FCMDevice.objects.all()

    if request.method == 'GET':

        return render(request,'home.html',{'devices':devices})
    if request.method == 'POST':

        messageTitle = request.POST.get('messageTitle')
        deviceId = request.POST.getlist('Id')
        message = request.POST.get('message')
        print(request.POST)
        if deviceId:
            for id in deviceId:
                device = FCMDevice.objects.get(pk=id)
                device.send_message(title=messageTitle, body=message, icon="", data={"test": "test"})

        return render(request,'home.html',{'devices':devices})


# def model_form_upload(request):
#     if request.method == 'POST':
#         form = DocumentForm(request.POST, request.FILES)
#         if form.is_valid():
#             form.save()
#             return redirect('home')
#     else:
#         form = DocumentForm()
#     return render(request,'upload.html',{'form':form})
